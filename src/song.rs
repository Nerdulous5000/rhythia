use bevy::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Resource, Debug, Serialize, Deserialize, Clone)]
pub struct Info {
    pub name: String,
    pub bpm: f32,
    pub beats_per_measure: u16,
    pub duration: f32,
}

mod Build {

    use super::*;
    pub struct InfoBuilder {
        pub name: String,
        pub bpm: f32,
        pub beats_per_measure: u16,
        pub duration: f32,
    }

    impl InfoBuilder {
        pub fn build(self) -> Info {
            return Info {
                name: self.name,
                bpm: self.bpm,
                beats_per_measure: self.beats_per_measure,
                duration: self.duration,
            };
        }

        pub fn bpm(mut self, x: f32) -> InfoBuilder {
            self.bpm = x;
            return self;
        }

        pub fn beats_per_measure(mut self, x: u16) -> InfoBuilder {
            self.beats_per_measure = x;
            return self;
        }

        pub fn duration(mut self, x: f32) -> InfoBuilder {
            self.duration = x;
            return self;
        }

        pub fn name(mut self, x: String) -> InfoBuilder {
            self.name = x;
            return self;
        }
    }
}

impl Info {
    pub fn new() -> Build::InfoBuilder {
        return Build::InfoBuilder {
            name: "Untitled".to_string(),
            bpm: 120.,
            beats_per_measure: 4,
            duration: 120.,
        };
    }
}

fn generate_arbitrary_song() -> Info {
    return Info::new().bpm(110.).duration(2.5 * 60.).build();
}

fn print_song(song_info: Res<Info>) {
    info!("{:?}", song_info);
}

pub struct Debug;

impl Plugin for Debug {
    fn build(&self, app: &mut App) {
        app.insert_resource(generate_arbitrary_song())
            .add_startup_system(print_song);
    }
}
