use super::timing;
use crate::song;
use bevy::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Component)]
struct Note {
    timestamp: f32,
    key: Key,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Key {
    Unknown,
    Q,
    W,
    E,
    R,
}

mod Build {
    use super::*;
    #[derive(Serialize, Deserialize, Debug)]
    pub struct NoteBuilder {
        pub beatstamp: timing::Beatstamp,
        pub key: Key,
        pub info: song::Info,
    }

    impl NoteBuilder {
        pub fn build(self) -> Note {
            return Note {
                timestamp: self
                    .beatstamp
                    .to_seconds(self.info.bpm, self.info.beats_per_measure),
                key: self.key,
            };
        }

        pub fn beatstamp(mut self, x: timing::Beatstamp) -> Self {
            self.beatstamp = x;
            return self;
        }

        pub fn measure(mut self, x: u16) -> Self {
            self.beatstamp.measure = x;
            return self;
        }

        pub fn beat(mut self, x: u16) -> Self {
            self.beatstamp.beat = x;
            return self;
        }

        pub fn fraction(mut self, x: f32) -> Self {
            self.beatstamp.fraction = x;
            return self;
        }

        pub fn key(mut self, x: Key) -> Self {
            self.key = x;
            return self;
        }
    }
}

impl Note {
    pub fn new(x: &song::Info) -> Build::NoteBuilder {
        return Build::NoteBuilder {
            beatstamp: timing::Beatstamp {
                measure: 0,
                beat: 0,
                fraction: 0.,
            },
            key: Key::Unknown,
            info: x.clone(),
        };
    }

    pub fn activate() -> bool {
        return true;
    }
}

#[derive(Resource)]
pub struct NoteQueue(std::vec::Vec<Note>);

// Arbitrary notes added on startup.
fn add_note(info: &Res<song::Info>, mut commands: Commands) {
    commands.spawn(Note::new(info).measure(4).beat(3).build());
}

pub struct Notes;

impl Plugin for Notes {
    fn build(&self, app: &mut App) {}
}
