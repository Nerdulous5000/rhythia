use bevy::prelude::*;
use bevy::sprite::MaterialMesh2dBundle;

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
) {
    commands.spawn(Camera2dBundle::default());
    commands.spawn(SpriteBundle {
        texture: asset_server.load("images/background.png"),
        transform: Transform::from_translation(Vec3::new(0., 0., 0.)),
        ..default()
    });

    // vertical lines
    for n in 0..4 {
        commands.spawn(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb(0.25, 0.25, 0.35),
                custom_size: Some(Vec2::new(5.0, 700.0)),
                ..default()
            },
            transform: Transform::from_translation(Vec3::new(n as f32 * 100. + -150., -0., 1.)),
            ..default()
        });
    }

    // horizontal line
    commands.spawn(SpriteBundle {
        sprite: Sprite {
            color: Color::rgb(0.25, 0.25, 0.75),
            custom_size: Some(Vec2::new(450.0, 5.0)),
            ..default()
        },
        transform: Transform::from_translation(Vec3::new(0., -300., 1.)),
        ..default()
    });

    // Circles
    commands.spawn(MaterialMesh2dBundle {
        mesh: meshes.add(shape::Circle::new(50.).into()).into(),
        material: materials.add(ColorMaterial::from(Color::PURPLE)),
        transform: Transform::from_translation(Vec3::new(-50., 0., 1.)),
        ..default()
    });
}

pub struct Graphics;

impl Plugin for Graphics {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup);
    }
}
