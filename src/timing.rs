use bevy::prelude::*;
use log::debug;
use serde::{Deserialize, Serialize};

#[derive(Component, Clone, Copy, Debug, Deserialize, Serialize)]
pub struct Beatstamp {
    pub measure: u16,
    pub beat: u16,
    pub fraction: f32,
}

impl Beatstamp {
    pub fn from_seconds(x: f32, bpm: f32, beats_per_measure: u16) -> Beatstamp {
        const MINUTES_TO_SECONDS: f32 = 1.0 / 60.0;
        let total_beat_precise = x * MINUTES_TO_SECONDS as f32 * bpm;

        let mut fraction = total_beat_precise;

        let mut beat = f32::floor(fraction) as u16;
        fraction = f32::fract(fraction);

        let measure = beat / beats_per_measure;
        beat %= beats_per_measure;
        return Beatstamp {
            measure: measure,
            beat: beat,
            fraction: fraction,
        };
    }

    pub fn to_seconds(&self, bpm: f32, beats_per_measure: u16) -> f32 {
        const SECONDS_TO_MINUTES: f32 = 60.0;

        let total_beat_precise =
            (self.measure * beats_per_measure + self.beat) as f32 + self.fraction;

        return total_beat_precise / bpm * SECONDS_TO_MINUTES;
    }
}

// Print out a "timestamp" in the form of beats.
fn debug_beat(time: Res<Time>) {
    debug!(
        "{:?}",
        Beatstamp::from_seconds(time.elapsed_seconds(), 120.0, 4)
    );
}

pub struct Debug;

impl Plugin for Debug {
    fn build(&self, app: &mut App) {
        app.add_system(debug_beat);
    }
}
