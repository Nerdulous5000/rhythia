mod background;
mod notes;
mod song;
mod timing;

#[allow(dead_code)]
use bevy::prelude::*;
use bevy::window::PresentMode;
use bevy_prototype_lyon::prelude::*;
// use env_logger::Env;
use log::debug;

fn debug_init() {
    info!("Initializing...");
}

#[derive(Component)]
struct Position {
    x: f32,
    y: f32,
}

impl Default for Position {
    fn default() -> Self {
        Position { x: 0.0, y: 0.0 }
    }
}

fn update_note_position(mut query: Query<(&mut Position, &timing::Beatstamp)>, time: Res<Time>) {
    for (mut position, beatstamp) in query.iter_mut() {
        position.y = beatstamp.to_seconds(120.0, 4);
    }
}

fn print_char_event_system(mut char_input_events: EventReader<ReceivedCharacter>) {
    for event in char_input_events.iter() {
        info!("{:?}: '{}'", event, event.char);
    }
}

fn main() {
    // env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();
    debug!("Debug logs enabled");
    info!("Info logs enabled");
    warn!("Warnings enabled");
    error!("Errors enabled");

    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "Rhythia".to_string(),
                width: 500.,
                height: 800.,
                present_mode: PresentMode::AutoVsync,
                ..default()
            },
            ..default()
        }))
        .add_plugin(ShapePlugin)
        .add_plugin(notes::Notes)
        .add_plugin(background::Graphics)
        .add_plugin(song::Debug)
        .add_plugin(timing::Debug)
        //
        .add_startup_system(debug_init)
        //
        .add_system(update_note_position)
        .add_system(print_char_event_system)
        //
        .run();
}
